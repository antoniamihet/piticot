﻿using Piticot_v1;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace Piticot_v1
{
    /*  internal class Board
     {

         public List<Cell> cellList = new List<Cell>();

        int[,] plansa = new int[,] {{1,1,1,1,1,1,1,1,1,0,2},
                               {1,0,0,0,0,0,0,0,1,0,2},
                               {1,0,1,1,1,1,1,0,1,0,1},
                               {1,0,1,0,0,0,1,0,1,0,1},
                               {1,0,1,0,0,0,1,0,1,0,1},
                               {1,0,1,0,0,0,1,0,1,0,1},
                               {1,0,1,0,0,0,1,0,1,0,1},
                               {1,0,1,0,0,0,0,0,1,0,1},
                               {1,0,1,1,1,1,1,1,1,0,1},
                               {1,0,0,0,0,0,0,0,0,0,1},
                               {1,1,1,1,1,1,1,1,1,1,1}
                             };


         int[,] pozitii = new int[,] { {2,10},{3,10},{4,10},{5,10},{6,10},{7,10},{8,10},{9,10},{10,10},
                                       {10,9},{10,8},{10,7},{10,6},{10,5},{10,4},{10,3},{10,2},{10,1},{10,0},
                                        {9,0},{8,0},{7,0},{6,0},{5,0},{4,0},{3,0},{2,0},{1,0},{0,0},
                                        {0,1},{0,2},{0,3},{0,4},{0,5},{0,6},{0,7},{0,8},
                                        {1,8},{2,8},{3,8},{4,8},{5,8},{6,8},{7,8},{8,8},
                                        {8,7},{8,6},{8,5},{8,4},{8,3},{8,2},
                                        {7,2},{6,2},{5,2},{4,2},{3,2},{2,2},
                                        {2,3},{2,4},{2,5},{2,6},
                                        {3,6},{4,6},{5,6},{6,6}};

         public Board()
         {
             cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));

         }

         private int GenerateRoute(int X, int Y)
         {
                 for (int i = 0; i < 10; i++)
                 {
                     Cell cell = new Cell
                     {
                         x = X,
                         y = Y,
                         color = Color.Lavender,

                     };
                     cellList.Add(cell);
                     X += Constants.cellWidth + 2;
                 }

                 return X;
             }

         public void Show(Form f)
         {
             foreach (Cell cell in cellList)
             {
                 cell.Show(f);
             }
             f.Refresh();
         }
     }*/

    internal class Board
    {
        //ComunityChest comunityChest = new ComunityChest();
        //Chance chance = new Chance();

        public List<Cell> cellList = new List<Cell>();

        public Board()
        {
            cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));
            //GenerateMockCells();
        }

        private void GenerateMockCells()
        {
            int X = Constants.cellColumnLeftDistance;
            int Y = Constants.cellRowTopDistance;

            X = GenerateFirstRow(X, Y);
            Y = GenerateLastColumn(X, Y);
            X = GenerateLastRow(X, Y);
            Y = GenerateFirstColumn(X, Y);

            //File.WriteAllText(@"mockBoard.json", JsonConvert.SerializeObject(cellList));
        }

        private int GenerateFirstColumn(int X, int Y)
        {
            for (int i = 30; i < 40; i++)
            {
                Cell cell = new Cell
                {
                    x = X,
                    y = Y,
                    color = Color.AliceBlue,
         
                };
                cellList.Add(cell);
                Y -= Constants.cellHeight + 2;
            }

            return Y;
        }

        private int GenerateLastRow(int X, int Y)
        {
            for (int i = 20; i < 30; i++)
            {
                Cell cell = new Cell
                {
                    x = X,
                    y = Y,
                    color = Color.LightSalmon,
                    
                };
                cellList.Add(cell);
                X -= Constants.cellWidth + 2;
            }

            return X;
        }

        private int GenerateLastColumn(int X, int Y)
        {
            for (int i = 10; i < 20; i++)
            {
                Cell cell = new Cell
                {
                    x = X,
                    y = Y,
                    color = Color.LightGreen,
                
                };
                cellList.Add(cell);
                Y += Constants.cellHeight + 2;
            }

            return Y;
        }

        private int GenerateFirstRow(int X, int Y)
        {
            for (int i = 0; i < 10; i++)
            {
                Cell cell = new Cell
                {
                    x = X,
                    y = Y,
                    color = Color.Lavender,
             
                };
                cellList.Add(cell);
                X += Constants.cellWidth + 2;
            }

            return X;
        }

        public void Show(Form f)
        {
            foreach (Cell cell in cellList)
            {
                cell.Show(f);
            }
            f.Refresh();
        }
    }
} 