﻿using Piticot_v1;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Piticot_v1
{
    internal class Cell
    {
        //       public int index;
        public int x, y;
        //       public int heigth, width;
        
         
        // CellTypes cellType;
        //        List <Tuple<int, int>> rentRange = new List<Tuple<int, int>>();
        //        Image image;

        //        Player owner;

        public Panel backgroundPanel;
        public Panel colorPanel;
        public Color color = new Color();

        internal void Show(Form f)
        {
            backgroundPanel = SetBackgroudPanelProperties();

           // Panel propertyNamePanel = SetPropertyNamePanelProperties();
           // backgroundPanel.Controls.Add(propertyNamePanel);

            colorPanel = SetColorPanelProperties();
            backgroundPanel.Controls.Add(colorPanel);

           // Panel moneyPanel = SetMoneyPanelProperties();
          //  backgroundPanel.Controls.Add(moneyPanel);

            f.Controls.Add(backgroundPanel);
        }

   /*     private Panel SetMoneyPanelProperties()
        {
            Panel moneyPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Dock = DockStyle.Bottom,

            };
   */
    //        Label moneyLabel = new Label();
 
   //         moneyLabel.Dock = DockStyle.Bottom;
   //         moneyLabel.TextAlign = ContentAlignment.MiddleCenter;
    //        moneyLabel.Font = new Font("Arial", 8);

    //        moneyPanel.Controls.Add(moneyLabel);

    //        return moneyPanel;
     //   }

     //   private Panel SetPropertyNamePanelProperties()
     //   {
    //        Panel propertyNamePanel = new Panel
    //        {
    //            BorderStyle = BorderStyle.Fixed3D,
    //            BackColor = Constants.cellBackgroundColor,
    //            Dock = DockStyle.Top,
 //               Height = Constants.cellHeight / 2
    //        };

      //      Label cellPropertyName = new Label();
      
     //       cellPropertyName.Dock = DockStyle.Fill;
      //      cellPropertyName.TextAlign = ContentAlignment.MiddleCenter;
      //      cellPropertyName.Font = new Font("Arial", 8);

     //       propertyNamePanel.Controls.Add(cellPropertyName);

     //       return propertyNamePanel;
     //   }

        private Panel SetColorPanelProperties()
        {
            Panel colorPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = color,
                Dock = DockStyle.Top,
                Height = Constants.cellHeight / 5
            };
            return colorPanel;
        }

        private Panel SetBackgroudPanelProperties()
        {
            Panel backgroundPanel = new Panel
            {
                BorderStyle = BorderStyle.Fixed3D,
                BackColor = Constants.cellBackgroundColor,
                Left = x,
                Top = y,
                Height = Constants.cellHeight,
                Width = Constants.cellWidth
            };
            return backgroundPanel;
        }
    }
}