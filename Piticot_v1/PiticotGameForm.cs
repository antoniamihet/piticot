﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Piticot_v1
{
    public partial class startingForm : Form
    {
        private Label playerNameLabel;
        private Label playerPositionLabel;
        private Button btnStart;
        PiticotGame game;
        public startingForm()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            game = new PiticotGame();
            game.Start(this);

        }

        internal void HidePlayer(Player player)
        {
            game.board.cellList[player.position].
                 colorPanel.Controls.Clear();
        }

        internal void ShowPlayer(Player player)
        {
            playerNameLabel.Text = player.name;

            playerPositionLabel.Text = player.position.ToString();

            game.board.cellList[player.position].
                colorPanel.Controls.Add(new Label { Text = player.name });
        }

        private void InitializeComponent()
        {
            this.playerNameLabel = new System.Windows.Forms.Label();
            this.playerPositionLabel = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // playerNameLabel
            // 
            this.playerNameLabel.AutoSize = true;
            this.playerNameLabel.Location = new System.Drawing.Point(408, 142);
            this.playerNameLabel.Name = "playerNameLabel";
            this.playerNameLabel.Size = new System.Drawing.Size(89, 17);
            this.playerNameLabel.TabIndex = 0;
            this.playerNameLabel.Text = "Player Name";
            // 
            // playerPositionLabel
            // 
            this.playerPositionLabel.AutoSize = true;
            this.playerPositionLabel.Location = new System.Drawing.Point(400, 199);
            this.playerPositionLabel.Name = "playerPositionLabel";
            this.playerPositionLabel.Size = new System.Drawing.Size(102, 17);
            this.playerPositionLabel.TabIndex = 1;
            this.playerPositionLabel.Text = "Player Position";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(411, 84);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // startingForm
            // 
            this.ClientSize = new System.Drawing.Size(898, 345);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.playerPositionLabel);
            this.Controls.Add(this.playerNameLabel);
            this.Name = "startingForm";
            this.Load += new System.EventHandler(this.startingForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void startingForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ai inceput jocul");
        }

        // private void label1_Click(object sender, EventArgs e)
        // {

        // }

        //  private void startingForm_Load(object sender, EventArgs e)
        //  {


        //  }
    }
}