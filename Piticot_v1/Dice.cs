﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piticot_v1
{
    internal class Dice
    {
        static Random r = new Random();

        public static int Roll()
        {
            return r.Next(1, 7);
        }
    }
}